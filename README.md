# linux-ck-renamer-Pacman-hook
 [file:///etc/pacman.d/hooks/linux-ck-ivybridge.hook]
 
It changes grub menu entries to appropriately display kernel version code.

Obviously, version code updates after each kernel update.
